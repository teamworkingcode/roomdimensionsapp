﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public abstract class AFuncType : IBuild
    {
        public abstract void BuildPlan();
        protected abstract void WriteAt(string s, int x, int y);
        protected abstract float Calculate(float x, float y);

        protected abstract float Calculate(float x, float y, float z);

    }
}
