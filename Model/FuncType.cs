﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class FuncType : AFuncType
    {
        protected int origRow = 5;
        protected int origCol = 5;
        public FuncType()
        {

        }

        public override void BuildPlan()
        {
            
        }

        protected override float Calculate(float x, float y)
        {
            return x * y;
        }

        protected override float Calculate(float x, float y, float z)
        {
            return x * y * z;
        }

        protected override void WriteAt(string s, int x, int y)
        {
            try
            {
                Console.SetCursorPosition(origCol + x, origRow + y);
                Console.Write(s);
            }
            catch (ArgumentOutOfRangeException e)
            {
                Console.Clear();
                Console.WriteLine(e.Message);
            }
        }


    }
}
