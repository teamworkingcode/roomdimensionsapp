﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class Build //: IBuild
    {
        public Build()
        {

        }


        public IBuild CreateType(string Create)
        {
            try
            {
                switch (Create.ToLower())
                {
                    case "area":
                        return new Area();
                    case "paintreq":
                        return new PaintRequired();
                    case "roomvol":
                        return new RoomVolume();
                    default:
                        return null;

                }
            }
            catch (Exception e)
            {

                throw e;
            }
        }
    }
}
