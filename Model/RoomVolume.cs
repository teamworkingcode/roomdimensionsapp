﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class RoomVolume: FuncType
    {
        public RoomVolume()
        {

        }

        public override void BuildPlan()
        {
            Console.Clear();
            origRow = Console.CursorTop;
            origCol = Console.CursorLeft;
            Console.ForegroundColor = ConsoleColor.DarkRed;
            WriteAt("VOLUME CALUCLATOR", 9, 3);
            Console.WriteLine(Environment.NewLine);
            //Left Wall
            WriteAt("+", 5, 6);
            WriteAt("|", 5, 7);
            WriteAt("|", 5, 8);
            WriteAt("|", 5, 9);
            WriteAt("|", 5, 10);
            WriteAt("|", 5, 11);
            WriteAt("|", 5, 12);
            WriteAt("|", 5, 13);
            WriteAt("|", 5, 14);
            WriteAt("+", 5, 15);


            //LeftEst
            WriteAt("/", 6, 5);



            //Topest Wall
            WriteAt("-",6, 5);
            WriteAt("-",7, 5);
            WriteAt("-",8, 5);
            WriteAt("-",9, 5);
            WriteAt("-",10, 5);
            WriteAt("-",11, 5);
            WriteAt("-",12, 5);
            WriteAt("-",13, 5);
            WriteAt("-",14, 5);
            WriteAt("-",15, 5);
            WriteAt("-",16, 5);
            WriteAt("-",17, 5);
            WriteAt("-",18, 5);
            WriteAt("-",19, 5);
            WriteAt("-",20, 5);
            WriteAt("-",21, 5);
            WriteAt("-",22, 5);
            WriteAt("-",23, 5);
            WriteAt("-",24, 5);
            WriteAt("-", 25, 5);
            WriteAt("-", 26, 5);
            WriteAt("-", 27, 5);




            //Top Wall
            WriteAt("-", 6, 6);
            WriteAt("-", 7, 6);
            WriteAt("-", 8, 6);
            WriteAt("-", 9, 6);
            WriteAt("-", 10, 6);
            WriteAt("-", 11, 6);
            WriteAt("-", 12, 6);
            WriteAt("-", 13, 6);
            WriteAt("-", 14, 6);
            WriteAt("-", 15, 6);
            WriteAt("-", 16, 6);
            WriteAt("-", 17, 6);
            WriteAt("-", 18, 6);
            WriteAt("-", 19, 6);
            WriteAt("-", 20, 6);
            WriteAt("-", 21, 6);
            WriteAt("-", 22, 6);
            WriteAt("-", 23, 6);
            WriteAt("-", 24, 6);
            WriteAt("-", 25, 6);


            //Right Wall
            WriteAt("+", 26, 6);
            WriteAt("|", 26, 7);
            WriteAt("|", 26, 8);
            WriteAt("|", 26, 9);
            WriteAt("|", 26, 10);
            WriteAt("|", 26, 11);
            WriteAt("|", 26, 12);
            WriteAt("|", 26, 13);
            WriteAt("|", 26, 14);
            WriteAt("+", 26, 15);

            WriteAt("/", 27, 6);


            //Right Wall - rightest point
            WriteAt("|", 28, 6);
            WriteAt("|", 28, 7);
            WriteAt("|", 28, 8);
            WriteAt("|", 28, 9);
            WriteAt("|", 28, 10);
            WriteAt("|", 28, 11);
            WriteAt("|", 28, 12);
            WriteAt("|", 28, 13);
            WriteAt("|", 28, 14);






            //Bottom Wall
            WriteAt("-", 6, 15);
            WriteAt("-", 7, 15);
            WriteAt("-", 8, 15);
            WriteAt("-", 9, 15);
            WriteAt("-", 10, 15);
            WriteAt("-", 11, 15);
            WriteAt("-", 12, 15);
            WriteAt("-", 13, 15);
            WriteAt("-", 14, 15);
            WriteAt("-", 15, 15);
            WriteAt("-", 16, 15);
            WriteAt("-", 17, 15);
            WriteAt("-", 18, 15);
            WriteAt("-", 19, 15);
            WriteAt("-", 20, 15);
            WriteAt("-", 21, 15);
            WriteAt("-", 22, 15);
            WriteAt("-", 23, 15);
            WriteAt("-", 24, 15);
            WriteAt("-", 25, 15);

            Console.ResetColor();

            Console.BackgroundColor = ConsoleColor.Green;
        }
    }
}
