﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class Area : FuncType
    {
        public Area()
        {
        }


        public void ClearCurrentConsoleLine(int cursorPos)
        {
            int currentLineCursor = Console.CursorTop;
            Console.SetCursorPosition(cursorPos, Console.CursorTop);
            Console.Write(new string(' ', Console.WindowWidth));
            Console.SetCursorPosition(cursorPos, currentLineCursor);
        }



        private void Instructions()
        {
            Console.ResetColor();

            WriteAt("Please Enter the Width of the Area of floor you wish to calculate (metres):", 0, 19);
            Console.WriteLine();
            var _width = Console.ReadLine();
            float width_res;
            float height_res;
            if (float.TryParse(_width, out width_res))
            {
                Console.WriteLine("You have provided a valid Value");
                Console.BackgroundColor = ConsoleColor.Green;
                WriteAt("Width: " + width_res.ToString(), 13, 17);
                Console.ResetColor();
                WriteAt("Thank you. Now Please enter the Height of the Area of floor", 0, 21);
                Console.WriteLine(Environment.NewLine);
                var _height = Console.ReadLine();
                if(float.TryParse(_height, out height_res))
                {
                    Console.BackgroundColor = ConsoleColor.Green;
                    WriteAt("Height: " + height_res.ToString(), 28, 11);
                    Console.ResetColor();
                    WriteAt("Press enter to calculate Area of the dimensions provided", 0, 23);
                    var entered = Console.ReadKey(true);
                    if(entered.Key == ConsoleKey.Enter)
                    {
                        Console.WriteLine(Environment.NewLine);
                        Console.WriteLine("The area of floor from the dimensions provided is: {0}", Calculate(width_res, height_res));
                    }
                }
            }
            else
            {
                Console.BackgroundColor = ConsoleColor.Red;
                Console.WriteLine("Something went wrong. please try again");
                Console.ResetColor();
                ClearCurrentConsoleLine(21);
                Instructions();
            }

        }

        public override void BuildPlan()
        {
            try
            {
                Console.Clear();
                origRow = Console.CursorTop;
                origCol = Console.CursorLeft;
                Console.ForegroundColor = ConsoleColor.DarkRed;
                WriteAt("AREA CALUCLATOR", 9, 3);
                Console.WriteLine(Environment.NewLine);
                //Left Wall
                WriteAt("+", 5, 6);
                WriteAt("|", 5, 7);
                WriteAt("|", 5, 8);
                WriteAt("|", 5, 9);
                WriteAt("|", 5, 10);
                WriteAt("|", 5, 11);
                WriteAt("|", 5, 12);
                WriteAt("|", 5, 13);
                WriteAt("|", 5, 14);
                WriteAt("+", 5, 15);

                //Top Wall
                WriteAt("-", 6, 6);
                WriteAt("-", 7, 6);
                WriteAt("-", 8, 6);
                WriteAt("-", 9, 6);
                WriteAt("-", 10, 6);
                WriteAt("-", 11, 6);
                WriteAt("-", 12, 6);
                WriteAt("-", 13, 6);
                WriteAt("-", 14, 6);
                WriteAt("-", 15, 6);
                WriteAt("-", 16, 6);
                WriteAt("-", 17, 6);
                WriteAt("-", 18, 6);
                WriteAt("-", 19, 6);
                WriteAt("-", 20, 6);
                WriteAt("-", 21, 6);
                WriteAt("-", 22, 6);
                WriteAt("-", 23, 6);
                WriteAt("-", 24, 6);
                WriteAt("-", 25, 6);


                //Right Wall
                WriteAt("+", 26, 6);
                WriteAt("|", 26, 7);
                WriteAt("|", 26, 8);
                WriteAt("|", 26, 9);
                WriteAt("|", 26, 10);
                WriteAt("|", 26, 11);
                WriteAt("|", 26, 12);
                WriteAt("|", 26, 13);
                WriteAt("|", 26, 14);
                WriteAt("+", 26, 15);





                //Bottom Wall
                WriteAt("-", 6, 15);
                WriteAt("-", 7, 15);
                WriteAt("-", 8, 15);
                WriteAt("-", 9, 15);
                WriteAt("-", 10, 15);
                WriteAt("-", 11, 15);
                WriteAt("-", 12, 15);
                WriteAt("-", 13, 15);
                WriteAt("-", 14, 15);
                WriteAt("-", 15, 15);
                WriteAt("-", 16, 15);
                WriteAt("-", 17, 15);
                WriteAt("-", 18, 15);
                WriteAt("-", 19, 15);
                WriteAt("-", 20, 15);
                WriteAt("-", 21, 15);
                WriteAt("-", 22, 15);
                WriteAt("-", 23, 15);
                WriteAt("-", 24, 15);
                WriteAt("-", 25, 15);

                Console.ResetColor();

                Console.BackgroundColor = ConsoleColor.Green;
            }
            catch (Exception e)
            {

                throw e;
            }
            finally
            {
                Instructions();
            }
        }
    }
}
