﻿using Model;
using RoomVolumeApp;
using RoomVolumeLogic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RoomVolumApp
{
    class Program
    {


        [STAThread]
        static void Main(string[] args)
        {


            IOption _options = new Options(new Build());
            Console.WriteLine("HELLO: {0}", Environment.MachineName);
            int userInput = 0;
            do
            {  
                userInput = _options.MenuOptions();
                Console.ResetColor();
                _options.DisplayMenu(userInput);

            } while (userInput != 1 && userInput != 2 && userInput != 3);
            Console.ReadLine();
        }
    }
}
