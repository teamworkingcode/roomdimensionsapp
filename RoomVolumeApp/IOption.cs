﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RoomVolumeLogic
{
    public interface IOption
    {
        int MenuOptions();
        void DisplayMenu(int selected);


    }
}
