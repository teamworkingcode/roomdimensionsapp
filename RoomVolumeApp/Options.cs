﻿using Model;
using RoomVolumeLogic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RoomVolumeApp
{
    public class Options : IOption
    {


        private Build _build;
        public Options(Build Build)
        {
            _build = Build;
        }


        public void DisplayMenu(int selected)
        {
            switch (selected)
            {
                case 1:

                    Console.BackgroundColor = ConsoleColor.White;
                    Console.ResetColor();
                    _build.CreateType("area").BuildPlan();
                    break;
                case 2:
                    Console.BackgroundColor = ConsoleColor.Green;
                    Console.WriteLine();
                    break;
                case 3:
                    Console.BackgroundColor = ConsoleColor.Green;
                    Console.ResetColor();
                    _build.CreateType("roomvol").BuildPlan();
                    break;
                default:
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.Clear();
                    Console.WriteLine("Selection does not exist. Please enter a valid option, before hiring me");
                    Console.ResetColor();
                    break;
            }
        }

        public int MenuOptions()
        {
            Console.WriteLine("Dimension Calculator");
            Console.WriteLine();
            Console.WriteLine("1. Area of Floor");
            Console.WriteLine("2. Amount of paint required to paint the walls");
            Console.WriteLine("3. Volume of the room");
            var result = Console.ReadLine();
            return Convert.ToInt32(result);
        }
    }
}
